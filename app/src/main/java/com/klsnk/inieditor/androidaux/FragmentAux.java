package com.klsnk.inieditor.androidaux;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;

public class FragmentAux extends Fragment {

    static public <F extends Fragment> Object getParent(F fragment) {
        FragmentActivity parentActivity = fragment.getActivity();
        Fragment parentFragment = fragment.getParentFragment();

        return (parentFragment == null) ? parentActivity : parentFragment;
    }
}
