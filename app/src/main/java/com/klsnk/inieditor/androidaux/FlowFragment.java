package com.klsnk.inieditor.androidaux;

import android.os.Bundle;
import android.support.v4.app.Fragment;

public class FlowFragment extends Fragment {
    public boolean receiveResult(int requestCode, Bundle resultData) {
        for (Fragment fragment : getChildFragmentManager().getFragments()) {
            if (FlowFragment.class.isAssignableFrom(fragment.getClass())) {
                FlowFragment flowFragment = (FlowFragment)fragment;
                if (flowFragment.receiveResult(requestCode, resultData)) {
                    return true;
                }
            }
        }

        return false;
    }
}

