package com.klsnk.inieditor.androidaux;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

public class FlowActivity extends AppCompatActivity {
    public boolean receiveResult(int requestCode, Bundle resultData) {
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            if (FlowFragment.class.isAssignableFrom(fragment.getClass())) {
                FlowFragment flowFragment = (FlowFragment)fragment;
                if (flowFragment.receiveResult(requestCode, resultData)) {
                    return true;
                }
            }
        }

        return false;
    }
}
