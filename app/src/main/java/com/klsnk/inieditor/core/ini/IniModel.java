package com.klsnk.inieditor.core.ini;

import com.klsnk.inieditor.core.ini.line.IniLine;
import com.klsnk.inieditor.core.ini.line.IniLineFacility;
import com.klsnk.inieditor.core.ini.line.SectionHeaderIniLine;
import com.klsnk.inieditor.core.text.Text;

import java.util.ArrayList;

public class IniModel implements IniLineList, IniSectionList {
    private class Section {
        public Section(int offset, String name) {
            this.offset = offset;
            this.name = name;
        }
        public int offset;
        public String name;
    }


    private Text text;
    private ArrayList<Section> sections;
    private OnLinesChangeListener listener;
    private OnSectionsChangeListener sectionsListener;

    public IniModel(Text text) {
        this.text = text;
        this.sections = new ArrayList<>();

        for (int i = 0; i < linesCount(); ++i) {
            IniLine iniLine = getLine(i);
            if (iniLine instanceof SectionHeaderIniLine) {
                SectionHeaderIniLine sectionHeaderIniLine = (SectionHeaderIniLine)iniLine;
                sections.add(new Section(i, sectionHeaderIniLine.getSectionName()));
            }
        }
    }

    public void setOnLinesChangeListener(OnLinesChangeListener listener) {
        this.listener = listener;
    }

    public void setOnSectionsChangeListener(OnSectionsChangeListener sectionsListener) {
        this.sectionsListener = sectionsListener;
    }

    public int linesCount() {
        return text.linesCount();
    }

    public IniLine getLine(int index) {
        return IniLineFacility.stringToIniLine(text.getLine(index));
    }

    public void replaceLine(int index, IniLine line) {
        IniLine oldLine = getLine(index);
        text.replaceLine(index, line.toString());
        if (listener != null) {
            listener.onReplaceLine(index, oldLine, line);
        }
    }

    public void addLine(IniLine line) {
        text.addLine(line.toString());
        if (listener != null) {
            listener.onAddLine(line);
        }
    }

    public void insertLine(int index, IniLine line) {
        text.insertLine(index, line.toString());
        shiftSectionsFrom(index, 1);
        if (listener != null) {
            listener.onInsertLine(index, line);
        }
    }

    public void removeLine(int index) {
        text.removeLine(index);
        shiftSectionsFrom(index, -1);
        if (listener != null) {
            listener.onRemoveLine(index);
        }
    }

    public int sectionsCount() {
        return sections.size();
    }

    public String getSectionName(int index) {
        return sections.get(index).name;
    }

    public void renameSection(int index, String name) {
        Section section = sections.get(index);
        String oldName = section.name;
        section.name = name;

        if (sectionsListener != null) {
            sectionsListener.onRenameSection(index, oldName, name);
        }
    }

    public IniLineList getSection(int index) {
        final int firstItemIndex = sectionFirstItemIndex(index);
        final int sectionSize = sectionSize(index);
        return new IniSection(this, firstItemIndex, sectionSize);
    }

    public IniLineList getFreeSection() {
        final int firstItemIndex = 0;
        final int sectionSize = sections.isEmpty() ? linesCount() : sections.get(0).offset;
        return new IniSection(this, firstItemIndex, sectionSize);
    }

    public void removeSection(int index) {
        final int sectionSize = sectionSize(index);
        final int firstItemIndex = sectionFirstItemIndex(index);

        for (int i = 0; i < sectionSize; ++i) {
            removeLine(firstItemIndex);
        }

        sections.remove(index);
        removeLine(firstItemIndex - 1);

        if (sectionsListener != null) {
            sectionsListener.onRemoveSection(index);
        }
    }

    public void addSection(String name) {
        addLine(new SectionHeaderIniLine(name));
        sections.add(new Section(linesCount() - 1, name));

        if (sectionsListener != null) {
            sectionsListener.onAddSection(name);
        }
    }

    public void insertSection(int sectionIndex, String name) {
        int offset = sections.get(sectionIndex).offset;
        insertLine(offset, new SectionHeaderIniLine(name));
        sections.add(sectionIndex, new Section(offset, name));

        if (sectionsListener != null) {
            sectionsListener.onInsertSection(sectionIndex, name);
        }
    }

    private int sectionFirstItemIndex(int sectionIndex) {
        return sections.get(sectionIndex).offset + 1;
    }

    private int sectionSize(int sectionIndex) {
        if (sectionIndex < (sectionsCount() - 1)) {
            return sections.get(sectionIndex + 1).offset - sectionFirstItemIndex(sectionIndex);
        } else {
            return linesCount() - sectionFirstItemIndex(sectionIndex);
        }
    }

    private void shiftSectionsFrom(int lineIndex, int shift) {
        for (int i = 0; i < sectionsCount(); ++i) {
            Section section = sections.get(i);
            if (section.offset >= lineIndex) {
                section.offset += shift;
            }
        }
    }
}
