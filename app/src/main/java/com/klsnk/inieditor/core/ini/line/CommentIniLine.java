package com.klsnk.inieditor.core.ini.line;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommentIniLine implements IniLine {
    private String prefixWhitespaces;
    private char commentSymbol;
    private String content;

    private static final Pattern LINE_PATTERN = Pattern.compile("^(\\s*)(;|#)(.*)$");

    private CommentIniLine() {}
    public CommentIniLine(String content) {
        prefixWhitespaces = "";
        commentSymbol = ';';
        this.content = content;
    }

    public static CommentIniLine fromUtf16String(String string) {
        Matcher matcher = LINE_PATTERN.matcher(string);
        if (matcher.find()) {
            CommentIniLine instance = new CommentIniLine();
            instance.prefixWhitespaces = matcher.group(1);
            instance.commentSymbol = matcher.group(2).charAt(0);
            instance.content = matcher.group(3);
            return instance;
        } else {
            return null;
        }
    }

    public String toString() {
        return prefixWhitespaces + commentSymbol + content;
    }

    public CommentIniLine clone() {
        CommentIniLine instance = new CommentIniLine();
        instance.prefixWhitespaces = prefixWhitespaces;
        instance.commentSymbol = commentSymbol;
        instance.content = content;
        return instance;
    }

    public char getCommentSymbol() {
        return commentSymbol;
    }

    public void setCommentSymbol(char commentSymbol) {
        this.commentSymbol = commentSymbol;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
