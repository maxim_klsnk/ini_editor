package com.klsnk.inieditor.core.text;

import java.util.ArrayList;

public interface Text {

    ArrayList<String> getWholeText();
    void replaceWholeText(ArrayList<String> text);

    int linesCount();
    String getLine(int index);
    void addLine(String line);
    void insertLine(int index, String line);
    void replaceLine(int index, String line);
    void removeLine(int index);
}
