package com.klsnk.inieditor.core.ini.line;

public class IniLineFacility {
    public static IniLine stringToIniLine(String string) {

        CommentIniLine commentIniLine = CommentIniLine.fromUtf16String(string);
        if (commentIniLine != null) {
            return commentIniLine;
        }

        SectionHeaderIniLine sectionHeaderIniLine = SectionHeaderIniLine.fromUtf16String(string);
        if (sectionHeaderIniLine != null) {
            return sectionHeaderIniLine;
        }

        KeyValueIniLine keyValueIniLine = KeyValueIniLine.fromUtf16String(string);
        if (keyValueIniLine != null) {
            return keyValueIniLine;
        }

        EmptyIniLine emptyIniLine = EmptyIniLine.fromUtf16String(string);
        if (emptyIniLine != null) {
            return emptyIniLine;
        }

        return InvalidIniLine.fromUtf16String(string);
    }
}
