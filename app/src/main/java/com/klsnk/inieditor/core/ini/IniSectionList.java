package com.klsnk.inieditor.core.ini;

public interface IniSectionList {
    interface OnSectionsChangeListener {
        void onRenameSection(int index, String oldName, String newName);
        void onAddSection(String name);
        void onInsertSection(int index, String name);
        void onRemoveSection(int index);
    }

    void setOnSectionsChangeListener(OnSectionsChangeListener listener);
    int sectionsCount();
    String getSectionName(int index);
    IniLineList getSection(int index);
    IniLineList getFreeSection();
    void renameSection(int index, String name);
    void removeSection(int index);
    void addSection(String name);
    void insertSection(int sectionIndex, String name);
}
