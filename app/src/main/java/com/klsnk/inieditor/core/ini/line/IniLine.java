package com.klsnk.inieditor.core.ini.line;

import java.io.Serializable;

public interface IniLine extends Serializable {
    String toString();
    IniLine clone();
}
