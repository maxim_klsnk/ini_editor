package com.klsnk.inieditor.core.ini;

import com.klsnk.inieditor.core.ini.line.IniLine;

public class IniSection implements IniLineList {
    private IniModel iniModel;
    private int firstItemIndex;
    private int sectionSize;
    private OnLinesChangeListener listener;

    public IniSection(IniModel iniModel, int firstItemIndex, int sectionSize) {
        this.iniModel = iniModel;
        this.firstItemIndex = firstItemIndex;
        this.sectionSize = sectionSize;
    }

    public void setOnLinesChangeListener(OnLinesChangeListener listener) {
        this.listener = listener;
    }

    public int linesCount() {
        return sectionSize;
    }

    public IniLine getLine(int index) {
        return iniModel.getLine(firstItemIndex + index);
    }

    public void replaceLine(int index, IniLine line) {
        IniLine oldLine = getLine(index);
        iniModel.replaceLine(firstItemIndex + index, line);
        if (listener != null) {
            listener.onReplaceLine(index, oldLine, line);
        }
    }

    public void addLine(IniLine line) {
        iniModel.insertLine(firstItemIndex + sectionSize, line);
        ++sectionSize;
        if (listener != null) {
            listener.onAddLine(line);
        }
    }

    public void insertLine(int index, IniLine line) {
        iniModel.insertLine(firstItemIndex + index, line);
        ++sectionSize;
        if (listener != null) {
            listener.onInsertLine(index, line);
        }
    }

    public void removeLine(int index) {
        iniModel.removeLine(firstItemIndex + index);
        --sectionSize;
        if (listener != null) {
            listener.onRemoveLine(index);
        }
    }
}
