package com.klsnk.inieditor.core.ini;

import com.klsnk.inieditor.core.ini.line.IniLine;

public interface IniLineList {
    interface OnLinesChangeListener {

        void onReplaceLine(int index, IniLine oldLine, IniLine newLine);
        void onAddLine(IniLine line);
        void onInsertLine(int index, IniLine line);
        void onRemoveLine(int index);
    }

    void setOnLinesChangeListener(OnLinesChangeListener listener);

    int linesCount();
    IniLine getLine(int index);
    void replaceLine(int index, IniLine line);
    void addLine(IniLine line);
    void insertLine(int index, IniLine line);
    void removeLine(int index);
}
