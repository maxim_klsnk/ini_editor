package com.klsnk.inieditor.core.ini.line;

public class InvalidIniLine implements IniLine {
    private String content;

    private InvalidIniLine() {}

    public static InvalidIniLine fromUtf16String(String string) {
        InvalidIniLine instance = new InvalidIniLine();
        instance.content = string;
        return instance;
    }

    public String toString() {
        return content;
    }

    public InvalidIniLine clone() {
        InvalidIniLine instance = new InvalidIniLine();
        instance.content = content;
        return instance;
    }

    public String getContent() {
        return content;
    }
}
