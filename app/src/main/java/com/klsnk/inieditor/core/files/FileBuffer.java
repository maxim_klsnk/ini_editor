package com.klsnk.inieditor.core.files;

import com.klsnk.inieditor.core.text.Text;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.ArrayList;

public class FileBuffer {

    public interface OnStateChangedListener {
        void stateChanged();
    }

    public interface OnNotEnoughMemoryListener {
        void onNotEnoughMemory();
    }

    private static final long minFreeMemoryInBytes = 10000000;
    private OnStateChangedListener onStateChangedListener;
    private OnNotEnoughMemoryListener onNotEnoughMemoryListener;
    private FileStreamCreator fileCreator;
    private Charset charset;

    ArrayList<String> buffer;
    boolean isNeedSave = false;

    private void stateChanged() {
        if (onStateChangedListener != null) {
            onStateChangedListener.stateChanged();
        }
    }

    boolean requireMemory(long bytesCount) {
        final Runtime runtime = Runtime.getRuntime();
        final long usedMemoryInBytes = runtime.totalMemory() - runtime.freeMemory();
        final long availableMemoryInBytes = runtime.maxMemory() - usedMemoryInBytes - minFreeMemoryInBytes;

        if (bytesCount > availableMemoryInBytes && onNotEnoughMemoryListener != null) {
            onNotEnoughMemoryListener.onNotEnoughMemory();
        }

        return bytesCount <= availableMemoryInBytes;
    }

    public Text getText() {
        return new FileBufferText(this);
    }

    public void setOnStateChangedListener(OnStateChangedListener listener) {
        onStateChangedListener = listener;
    }

    public void setOnNotEnoughMemoryListener(OnNotEnoughMemoryListener listener) {
        onNotEnoughMemoryListener = listener;
    }

    public boolean create(Charset charset) {
        this.charset = charset;
        fileCreator = null;
        buffer = new ArrayList<>();
        stateChanged();
        return true;
    }

    public boolean open(FileStreamCreator fileCreator, Charset charset) {

        try (BufferedReader in = new BufferedReader(
                new InputStreamReader(fileCreator.openInputStream(), charset))) {

            ArrayList<String> buffer = new ArrayList<>();
            String line;
            while ((line = in.readLine()) != null) {
                buffer.add(line);
            }

            this.fileCreator = fileCreator;
            this.buffer = buffer;
            this.charset = charset;

            stateChanged();
            return true;

        } catch (IOException exception) {
            return false;
        }
    }

    public boolean isOpen() {
        return buffer != null;
    }
    public boolean isNeedSave() {return isNeedSave;}

    public String fileName() {
        return fileCreator == null ? null : fileCreator.fileName();
    }

    public boolean save() {
        if (buffer == null || fileCreator == null) {
            return false;
        }

        try (BufferedWriter out = new BufferedWriter(new OutputStreamWriter(fileCreator.openOutputStream(), charset))) {
            for (String line : buffer) {
                out.write(line);
                out.newLine();
            }
            isNeedSave = false;
            stateChanged();
            return true;
        } catch (IOException exception) {
            return false;
        }
    }

    public boolean saveAs(FileStreamCreator fileCreator, Charset charset) {
        if (buffer == null) {
            return false;
        }

        FileStreamCreator previousFileCreator = this.fileCreator;
        Charset previousCharset = this.charset;

        this.fileCreator = fileCreator;
        this.charset = charset;

        if (!save()) {
            this.fileCreator = previousFileCreator;
            this.charset = previousCharset;
            return false;
        }

        return true;
    }

    public void close() {
        if (buffer == null) {
            return;
        }

        fileCreator = null;
        buffer = null;
        charset = null;
        stateChanged();
    }
}
