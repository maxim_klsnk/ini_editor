package com.klsnk.inieditor.core.ini.line;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SectionHeaderIniLine implements IniLine {
    private String sectionName;
    private String prefixWhitespaces;
    private String suffixWhitespaces;

    private static final Pattern LINE_PATTERN = Pattern.compile("^(\\s*)\\[([^]]+)](\\s*)$");

    private SectionHeaderIniLine() {}
    public SectionHeaderIniLine(String sectionName) {
        this.sectionName = sectionName;
        prefixWhitespaces = "";
        suffixWhitespaces = "";
    }

    public static SectionHeaderIniLine fromUtf16String(String string) {
        Matcher matcher = LINE_PATTERN.matcher(string);
        if (matcher.find()) {
            SectionHeaderIniLine instance = new SectionHeaderIniLine();
            instance.sectionName = matcher.group(2);
            instance.prefixWhitespaces = matcher.group(1);
            instance.suffixWhitespaces = matcher.group(3);
            return instance;
        } else {
            return null;
        }
    }

    public String toString() {
        return prefixWhitespaces + "[" + sectionName + "]" + suffixWhitespaces;
    }

    public SectionHeaderIniLine clone() {
        SectionHeaderIniLine instance = new SectionHeaderIniLine();
        instance.sectionName = sectionName;
        instance.prefixWhitespaces = prefixWhitespaces;
        instance.suffixWhitespaces = suffixWhitespaces;
        return instance;
    }

    public String getSectionName() {
        return sectionName;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }
}
