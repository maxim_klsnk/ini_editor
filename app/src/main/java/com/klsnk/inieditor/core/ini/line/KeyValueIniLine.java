package com.klsnk.inieditor.core.ini.line;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class KeyValueIniLine implements IniLine {
    private String key;
    private String value;

    private static final Pattern LINE_PATTERN = Pattern.compile("^([^=]*)=(.*)$");

    private KeyValueIniLine() {}

    public KeyValueIniLine(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public static KeyValueIniLine fromUtf16String(String string) {
        Matcher matcher = LINE_PATTERN.matcher(string);
        if (matcher.find()) {
            KeyValueIniLine instance = new KeyValueIniLine();
            instance.key = matcher.group(1);
            instance.value = matcher.group(2);
            return instance;
        } else {
            return null;
        }
    }

    public String toString() {
        return key + '=' + value;
    }

    public KeyValueIniLine clone() {
        KeyValueIniLine instance = new KeyValueIniLine();
        instance.key = key;
        instance.value = value;
        return instance;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
