package com.klsnk.inieditor.core.files;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public interface FileStreamCreator {
    InputStream openInputStream() throws IOException;
    OutputStream openOutputStream() throws IOException;
    String fileName();
}
