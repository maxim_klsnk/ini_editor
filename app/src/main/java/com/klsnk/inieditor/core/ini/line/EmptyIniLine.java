package com.klsnk.inieditor.core.ini.line;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmptyIniLine implements IniLine {
    private String whitespaces;

    private static final Pattern LINE_PATTERN = Pattern.compile("^\\s*$");

    public EmptyIniLine() {
        whitespaces = "";
    }

    public static EmptyIniLine fromUtf16String(String string) {
        Matcher matcher = LINE_PATTERN.matcher(string);
        if (matcher.find()) {
            EmptyIniLine instance = new EmptyIniLine();
            instance.whitespaces = matcher.group();
            return instance;
        } else {
            return null;
        }
    }

    public String toString() {
        return whitespaces;
    }

    public EmptyIniLine clone() {
        EmptyIniLine instance = new EmptyIniLine();
        instance.whitespaces = whitespaces;
        return instance;
    }
}
