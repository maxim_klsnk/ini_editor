package com.klsnk.inieditor.core.files;

import com.klsnk.inieditor.core.text.Text;

import java.util.ArrayList;

class FileBufferText implements Text {
    FileBuffer fileBuffer;

    FileBufferText(FileBuffer fileBuffer) {
        this.fileBuffer = fileBuffer;
    }

    public void replaceWholeText(ArrayList<String> text) {
        fileBuffer.buffer = new ArrayList<>(text);
    }

    public ArrayList<String> getWholeText() {
        return new ArrayList<>(fileBuffer.buffer);
    }

    public int linesCount() {
        return fileBuffer.buffer.size();
    }

    public String getLine(int index) {
        return fileBuffer.buffer.get(index);
    }

    public void addLine(String line) {
        fileBuffer.buffer.add(line);
        fileBuffer.isNeedSave = true;
    }

    public void insertLine(int index, String line) {
        fileBuffer.buffer.add(index, line);
        fileBuffer.isNeedSave = true;
    }

    public void replaceLine(int index, String line) {
        fileBuffer.buffer.set(index, line);
        fileBuffer.isNeedSave = true;
    }

    public void removeLine(int index) {
        fileBuffer.buffer.remove(index);
        fileBuffer.isNeedSave = true;
    }
}

