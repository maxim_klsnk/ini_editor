package com.klsnk.inieditor.ui.inilinedialog.inilinefragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.klsnk.inieditor.R;
import com.klsnk.inieditor.core.ini.line.IniLine;
import com.klsnk.inieditor.core.ini.line.KeyValueIniLine;

public class KeyValueIniLineFragment extends IniLineFragment {

    private static final String KEY_ARG = "key";
    private static final String VALUE_ARG = "value";

    private EditText keyEditText;
    private EditText valueEditText;

    public void setIniLine(KeyValueIniLine keyValueIniLine) {
        Bundle arguments = new Bundle();
        arguments.putString(KEY_ARG, keyValueIniLine.getKey());
        arguments.putString(VALUE_ARG, keyValueIniLine.getValue());
        setArguments(arguments);
    }

    public IniLine validate() {
        String key = keyEditText.getText().toString();
        String value = valueEditText.getText().toString();
        boolean ok = true;

        if (key.isEmpty()) {
            keyEditText.setError(getString(R.string.keyIniLineError));
            ok = false;
        }

        if (value.isEmpty()) {
            valueEditText.setError(getString(R.string.valueIniLineError));
            ok = false;
        }

        return ok ? new KeyValueIniLine(key, value) : null;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_key_value_ini_line, container, false);

        keyEditText = (EditText)view.findViewById(R.id.ini_line_key_edit_text);
        valueEditText = (EditText)view.findViewById(R.id.ini_line_value_edit_text);

        Bundle arguments = getArguments();
        if (arguments != null) {
            keyEditText.setText(arguments.getString(KEY_ARG), TextView.BufferType.EDITABLE);
            valueEditText.setText(arguments.getString(VALUE_ARG), TextView.BufferType.EDITABLE);
        }

        return view;
    }
}
