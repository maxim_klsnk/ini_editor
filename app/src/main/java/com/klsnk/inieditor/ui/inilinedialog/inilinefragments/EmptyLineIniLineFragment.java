package com.klsnk.inieditor.ui.inilinedialog.inilinefragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.klsnk.inieditor.R;
import com.klsnk.inieditor.core.ini.line.EmptyIniLine;
import com.klsnk.inieditor.core.ini.line.IniLine;

public class EmptyLineIniLineFragment extends IniLineFragment {

    public IniLine validate() {
        return new EmptyIniLine();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_empty_line_ini_line, container, false);
    }
}
