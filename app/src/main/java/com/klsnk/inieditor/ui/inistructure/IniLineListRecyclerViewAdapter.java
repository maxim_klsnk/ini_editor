package com.klsnk.inieditor.ui.inistructure;

import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.klsnk.inieditor.R;
import com.klsnk.inieditor.core.ini.IniLineList;
import com.klsnk.inieditor.core.ini.line.CommentIniLine;
import com.klsnk.inieditor.core.ini.line.EmptyIniLine;
import com.klsnk.inieditor.core.ini.line.IniLine;
import com.klsnk.inieditor.core.ini.line.InvalidIniLine;
import com.klsnk.inieditor.core.ini.line.KeyValueIniLine;
import com.klsnk.inieditor.core.ini.line.SectionHeaderIniLine;

public class IniLineListRecyclerViewAdapter extends RecyclerView.Adapter<IniLineListRecyclerViewAdapter.ViewHolder> {

    public interface InteractionListener {
        void onItemClick(int position);
        void onItemRemove(int position);
        void onItemEdit(int position);
        void onItemInsertNew(int position);
        void onItemsSetChanged();
    }

    private final IniLineList iniLineList;
    private final InteractionListener interactionListener;
    private View menuButton;

    IniLineListRecyclerViewAdapter(IniLineList iniLineList, InteractionListener interactionListener) {
        this.iniLineList = iniLineList;
        this.interactionListener = interactionListener;

        iniLineList.setOnLinesChangeListener(new IniLineList.OnLinesChangeListener() {
            @Override
            public void onAddLine(IniLine line) {
                notifyItemInserted(iniLineList.linesCount() - 1);
                interactionListener.onItemsSetChanged();
            }

            @Override
            public void onInsertLine(int index, IniLine line) {
                notifyItemInserted(index);
                notifyItemRangeChanged(index + 1, iniLineList.linesCount());
                interactionListener.onItemsSetChanged();
            }

            @Override
            public void onReplaceLine(int index, IniLine oldLine, IniLine newLine) {
                notifyItemChanged(index);
            }

            @Override
            public void onRemoveLine(int index) {
                notifyItemRemoved(index);
                notifyItemRangeChanged(index, iniLineList.linesCount());
                interactionListener.onItemsSetChanged();
            }
        });
    }

    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_ini_line, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        IniLine iniLine = iniLineList.getLine(position);
        String lineText = "";
        String subLineText = "";

        Resources resources = holder.view.getContext().getResources();

        if (iniLine instanceof InvalidIniLine) {
            lineText = resources.getString(R.string.invalidLineHeaderText);
        } else if (iniLine instanceof EmptyIniLine) {
            lineText = resources.getString(R.string.emptyLineHeaderText);
        } else if (iniLine instanceof CommentIniLine) {
            lineText = resources.getString(R.string.commentLineHeaderText);
            subLineText = ((CommentIniLine) iniLine).getContent();
        } else if (iniLine instanceof KeyValueIniLine) {
            KeyValueIniLine keyValue = (KeyValueIniLine)iniLine;
            lineText = keyValue.getKey();
            subLineText = keyValue.getValue();
        }

        holder.lineTextView.setText(lineText);
        holder.subLineTextView.setText(subLineText);

        holder.view.setOnLongClickListener((View) -> {
            openItemPopup(holder, position);
            return true;
        });

        holder.view.setOnClickListener((View) -> interactionListener.onItemClick(position));
        holder.menuButton.setOnClickListener((View view) -> openItemPopup(holder, position));
    }

    public void openItemPopup(ViewHolder holder, int position) {
        PopupMenu popup = new PopupMenu(holder.menuButton.getContext(), holder.menuButton);
        popup.inflate(R.menu.menu_ini_line_item_options);
        popup.setOnMenuItemClickListener((MenuItem item) -> {
            switch (item.getItemId()) {
                case R.id.action_ini_line_options_edit: {
                    interactionListener.onItemEdit(position);
                    break;
                }

                case R.id.action_ini_line_options_insert_new: {
                    interactionListener.onItemInsertNew(position);
                    break;
                }

                case R.id.action_ini_line_options_remove: {
                    interactionListener.onItemRemove(position);
                    break;
                }
            }
            return true;
        });
        popup.show();
    }

    @Override
    public int getItemCount() {
        return iniLineList.linesCount();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final View view;
        final TextView lineTextView;
        final TextView subLineTextView;
        final View menuButton;

        ViewHolder(View view) {
            super(view);
            this.view = view;
            this.lineTextView = view.findViewById(R.id.item_text);
            this.subLineTextView = view.findViewById(R.id.sub_item_text);
            this.menuButton = view.findViewById(R.id.button_ini_line_menu);
        }
    }
}
