package com.klsnk.inieditor.ui.inilinedialog;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.klsnk.inieditor.R;
import com.klsnk.inieditor.core.ini.line.IniLine;
import com.klsnk.inieditor.ui.inilinedialog.inilinefragments.IniLineFragment;
import com.klsnk.inieditor.ui.inilinedialog.inilinefragments.IniLineFragmentFacility;

public class IniLineEditDialogActivity extends AppCompatActivity {

    public enum ActionType { EDIT, INSERT, ADD }
    public static final String INI_LINE_KEY = "INI_LINE_KEY";
    public static final String ACTION_TYPE_KEY = "ACTION_TYPE_KEY";
    public static final int RESULT_OK = 223;
    public static final int RESULT_CANCEL = 558;

    private IniLine iniLineToEdit;
    private ActionType actionType;
    private IniLineFragment iniLineFragment;

    public void onIniLineTypeSelected(int index) {
        if (actionType == ActionType.EDIT
                && IniLineFragmentFacility.getTitleIndexByIniLine(iniLineToEdit) == index) {
            iniLineFragment = IniLineFragmentFacility.getFragmentByIniLine(iniLineToEdit);
        } else {
            iniLineFragment = IniLineFragmentFacility.getFragmentByTitleIndex(index);
        }
        IniLineFragmentFacility.getFragmentByTitleIndex(index);

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.ini_line_fragment_wrapper, iniLineFragment);
        fragmentTransaction.commit();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle arguments = getIntent().getExtras();
        actionType = (ActionType)arguments.getSerializable(ACTION_TYPE_KEY);
        if (actionType == ActionType.EDIT) {
            iniLineToEdit = (IniLine) arguments.getSerializable(INI_LINE_KEY);
        }

        setContentView(R.layout.dialog_ini_line_edit);
        Toolbar toolbar = findViewById(R.id.dialog_ini_line_edit_toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_baseline_close_24px);

        String title;
        switch (actionType) {
            case EDIT: {
                title = getString(R.string.editIniLineDialogTitle);
                break;
            }

            case INSERT: {
                title = getString(R.string.insertIniLineDialogTitle);
                break;
            }

            case ADD: {
                title = getString(R.string.addIniLineDialogTitle);
                break;
            }

            default: {
                title = "";
            }
        }

        actionBar.setTitle(title);

        Spinner iniLineTypeSpinner = findViewById(R.id.ini_line_type_spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                this,
                R.array.iniLineFragmentTitles,
                android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
        iniLineTypeSpinner.setAdapter(adapter);


        iniLineTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                onIniLineTypeSelected(pos);
            }
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        if (iniLineToEdit != null) {
            iniLineTypeSpinner.setSelection(IniLineFragmentFacility.getTitleIndexByIniLine(iniLineToEdit));
        } else {
            iniLineTypeSpinner.setSelection(0);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_ini_line_edit_dialog, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_ini_line_edit) {
            IniLine iniLine = iniLineFragment.validate();
            if (iniLine != null) {
                Intent resultIntent = new Intent();
                resultIntent.putExtra(INI_LINE_KEY, iniLine);
                resultIntent.putExtra(ACTION_TYPE_KEY, actionType);
                setResult(RESULT_OK, resultIntent);
                finish();
            }
            return true;
        } else if (id == android.R.id.home) {
            setResult(RESULT_CANCEL, null);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCEL, null);
        super.onBackPressed();
    }
}
