package com.klsnk.inieditor.ui.inistructure;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;

import com.klsnk.inieditor.R;
import com.klsnk.inieditor.androidaux.FlowActivity;

public class IniSectionNameDialog extends DialogFragment {
    public static final String TITLE_KEY = "TITLE_KEY";
    public static final String VALUE_KEY = "VALUE_KEY";

    TextInputEditText nameEditText;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Bundle arguments = getArguments();
        final int requestCode = getTargetRequestCode();

        AlertDialog.Builder builder = new AlertDialog.Builder(requireActivity());
        builder.setTitle(arguments.getString(TITLE_KEY));

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_ini_section_name, null);

        nameEditText = view.findViewById(R.id.dialog_section_name_edit_text);
        nameEditText.setText(arguments.getString(VALUE_KEY));


        builder.setView(view);

        builder.setPositiveButton(
                R.string.okInputDialogButtonText,
                (DialogInterface dialog, int id) -> {
            Bundle resultData = new Bundle();
            resultData.putString(VALUE_KEY, nameEditText.getText().toString());
            FlowActivity activity = (FlowActivity)getActivity();
            activity.receiveResult(requestCode, resultData);
            dismiss();
        });


        builder.setNegativeButton(
                R.string.cancelInputDialogButtonText,
                (DialogInterface dialog, int id) -> dismiss() );

        return builder.create();
    }
}
