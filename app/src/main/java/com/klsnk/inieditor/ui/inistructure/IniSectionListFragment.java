package com.klsnk.inieditor.ui.inistructure;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.klsnk.inieditor.R;
import com.klsnk.inieditor.core.ini.IniLineList;
import com.klsnk.inieditor.core.ini.IniSectionList;
import com.klsnk.inieditor.androidaux.FlowFragment;
import com.klsnk.inieditor.androidaux.FragmentAux;

public class IniSectionListFragment
        extends FlowFragment
        implements IniSectionListRecyclerViewAdapter.InteractionListener {

    public interface FragmentContext {
        IniSectionList getIniSectionList();
        void onShowIniLineList(IniLineList iniLineList);
        void onOpenSection(String sectionName);
    }

    private static final int RENAME_SECTION = 333;
    private static final int INSERT_SECTION = 334;
    private static final int ADD_SECTION = 335;

    private IniSectionList iniSectionList;
    private FragmentContext fragmentContext;
    private int targetIniSectionIndex = -1;

    public IniSectionListFragment() {
    }

    public void onAddButton() {
        onItemAdd();
    }

    @Override
    public boolean receiveResult(int requestCode, Bundle resultData) {

        final String iniSectionName = resultData.getString(IniSectionNameDialog.VALUE_KEY);

        switch (requestCode) {
            case RENAME_SECTION: {
                iniSectionList.renameSection(targetIniSectionIndex, iniSectionName);
                return true;
            }

            case INSERT_SECTION: {
                iniSectionList.insertSection(targetIniSectionIndex, iniSectionName);
                return true;
            }

            case ADD_SECTION: {
                iniSectionList.addSection(iniSectionName);
                return true;
            }

            default: {
                return false;
            }
        }
    }

    public void onFirstItemClick() {
        fragmentContext.onShowIniLineList(iniSectionList.getFreeSection());
        fragmentContext.onOpenSection(getString(R.string.freeSectionHeaderText));
    }

    public void onItemClick(int position) {
        fragmentContext.onShowIniLineList(iniSectionList.getSection(position));
        String sectionName = iniSectionList.getSectionName(position);
        fragmentContext.onOpenSection(sectionName);
    }

    public void onItemRename(int position) {
        showSectionDialog(position, RENAME_SECTION);
    }

    public void onItemInsertNew(int position) {
        showSectionDialog(position, INSERT_SECTION);
    }

    public void onItemAdd() {
        showSectionDialog(-1, ADD_SECTION);
    }

    public void onItemRemove(int position) {
        iniSectionList.removeSection(position);
    }

    public void showSectionDialog(int iniSectionIndex, int requestCode) {
        targetIniSectionIndex = iniSectionIndex;

        Bundle arguments = new Bundle();

        if (requestCode == RENAME_SECTION) {
            String sectionName = iniSectionList.getSectionName(iniSectionIndex);
            arguments.putString(IniSectionNameDialog.VALUE_KEY, sectionName);
        }

        String title = "";
        switch (requestCode) {
            case RENAME_SECTION: {
                title = getString(R.string.renameSectionDialogTitle);
                break;
            }

            case INSERT_SECTION: {
                title = getString(R.string.insertSectionDialogTitle);
                break;
            }

            case ADD_SECTION: {
                title = getString(R.string.addSectionDialogTitle);
                break;
            }
        }

        arguments.putString(IniSectionNameDialog.TITLE_KEY, title);

        IniSectionNameDialog iniSectionNameDialog = new IniSectionNameDialog();
        iniSectionNameDialog.setTargetFragment(null, requestCode);
        iniSectionNameDialog.setArguments(arguments);

        iniSectionNameDialog.show(getActivity().getSupportFragmentManager(), IniSectionNameDialog.class.getName());
    }

    @SuppressWarnings("unused")
    public static IniSectionListFragment newInstance(int columnCount) {
        return new IniSectionListFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ini_section_list, container, false);

        fragmentContext = (FragmentContext) FragmentAux.getParent(this);

        iniSectionList = fragmentContext.getIniSectionList();

        Context context = view.getContext();
        RecyclerView recyclerView = view.findViewById(R.id.ini_section_list_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(new IniSectionListRecyclerViewAdapter(iniSectionList, this));

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                LinearLayoutManager.VERTICAL);
        recyclerView.addItemDecoration(dividerItemDecoration);

        return view;
    }
}

