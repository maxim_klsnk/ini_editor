package com.klsnk.inieditor.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.klsnk.inieditor.R;

public class StartFragment extends Fragment {

    public interface FragmentContext {
        void onNewFile();
        void onOpenFile();
    }

    private FragmentContext fragmentContext;

    public StartFragment() {
    }

    @SuppressWarnings("unused")
    public static StartFragment newInstance() {
        return new StartFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_start, container, false);

        AppCompatButton newFileButton = view.findViewById(R.id.start_new_file_button);
        newFileButton.setOnClickListener((View v) -> fragmentContext.onNewFile());

        AppCompatButton openFileButton = view.findViewById(R.id.start_open_file_button);
        openFileButton.setOnClickListener((View v) -> fragmentContext.onOpenFile());

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        fragmentContext = (FragmentContext) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        fragmentContext = null;
    }
}
