package com.klsnk.inieditor.ui;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.OpenableColumns;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import com.klsnk.inieditor.R;
import com.klsnk.inieditor.core.files.FileBuffer;
import com.klsnk.inieditor.core.text.Text;
import com.klsnk.inieditor.androidaux.FlowActivity;
import com.klsnk.inieditor.ui.inistructure.IniStructureFragment;

public class MainActivity extends FlowActivity
        implements StartFragment.FragmentContext, IniStructureFragment.FragmentContext {

    private static final int REQUEST_NONE = 0;
    private static final int REQUEST_TEXT_FILE_OPEN = 1;
    private static final int REQUEST_TEXT_FILE_SAVE = 2;
    private static final int REQUEST_CHARSET_FOR_FILE_OPEN = 324234;
    private static final int REQUEST_CHARSET_FOR_FILE_NEW = 293090;
    private static final Charset[] standardCharsetList = {
        StandardCharsets.ISO_8859_1,
        StandardCharsets.US_ASCII,
        StandardCharsets.UTF_8,
        StandardCharsets.UTF_16,
        StandardCharsets.UTF_16BE,
        StandardCharsets.UTF_16LE
    };

    private Charset charset = StandardCharsets.UTF_8;
    private FileBuffer fileBuffer = new FileBuffer();
    private Uri currentFileUri;
    private int lastClosedActivityRequestCode = REQUEST_NONE;
    private Menu fileMenu;
    private IniStructureFragment iniStructureFragment;

    private void showCharsetDialog(int requestCode) {
        ArrayList<String> charsetNameList = new ArrayList<>();
        for (final Charset charset : standardCharsetList) {
            charsetNameList.add(charset.name());
        }

        Bundle arguments = new Bundle();
        arguments.putStringArrayList(CharsetDialog.CHARSET_NAME_LIST_KEY, charsetNameList);

        CharsetDialog charsetDialog = new CharsetDialog();
        charsetDialog.setArguments(arguments);
        charsetDialog.setTargetFragment(null, requestCode);
        charsetDialog.show(getSupportFragmentManager(), CharsetDialog.class.getName());
    }

    @Override
    public boolean receiveResult(int requestCode, Bundle resultData) {
        switch (requestCode) {
            case REQUEST_CHARSET_FOR_FILE_OPEN: {
                int charsetIndex = resultData.getInt(CharsetDialog.SELECTED_CHARSET_INDEX_KEY);
                charset = standardCharsetList[charsetIndex];

                onCloseFile();
                if (fileBuffer.open(
                        new AndroidFileStreamCreator(getBaseContext(), currentFileUri), charset)) {
                    showSnackbar(getString(R.string.openFileSnackbarText));
                } else {
                    showSnackbar(getString(R.string.failOpenFileSnackbarText));
                }
                return true;
            }

            case REQUEST_CHARSET_FOR_FILE_NEW: {
                int charsetIndex = resultData.getInt(CharsetDialog.SELECTED_CHARSET_INDEX_KEY);
                charset = standardCharsetList[charsetIndex];

                onCloseFile();
                if (fileBuffer.create(charset)) {
                    showSnackbar(getString(R.string.newFileSnackbarText));
                } else {
                    showSnackbar(getString(R.string.failNewFileSnackbarText));
                }
                return true;
            }

            default: {
                return super.receiveResult(requestCode, resultData);
            }
        }
    }

    public void onOpenSection() {
        updateUi();
    }

    public void onCloseSection() {
        updateUi();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.activity_main_toolbar);
        setSupportActionBar(toolbar);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.activity_main_main_frame, new StartFragment());
        transaction.commit();

        fileBuffer.setOnStateChangedListener(this::updateUi);

        FloatingActionButton floatingActionButton = findViewById(R.id.fab);
        floatingActionButton.setOnClickListener((View) -> {
            if (iniStructureFragment != null) {
                iniStructureFragment.onAddButton();
            }
        });

        Intent intent = getIntent();
        String intentAction = intent.getAction();
        if (intentAction != null) {
            if (intentAction.equals(Intent.ACTION_VIEW)
                    || intentAction.equals(Intent.ACTION_EDIT)) {
                currentFileUri = intent.getData();
                showCharsetDialog(REQUEST_CHARSET_FOR_FILE_OPEN);
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (iniStructureFragment != null && iniStructureFragment.isRequireNavigateBack()) {
            iniStructureFragment.navigateBack();
        } else if (getSupportFragmentManager().getBackStackEntryCount() == 0 && fileBuffer.isOpen()) {
            fileBuffer.close();
        } else {
            super.onBackPressed();
        }
    }

    private void updateUi() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        FloatingActionButton floatingActionButton = findViewById(R.id.fab);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_baseline_arrow_back_24px);

        if (fileBuffer.isOpen()) {
            floatingActionButton.show();

            if (iniStructureFragment == null) {
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                iniStructureFragment = new IniStructureFragment();
                transaction.replace(R.id.activity_main_main_frame, iniStructureFragment);
                transaction.commit();
            }

            actionBar.setDisplayHomeAsUpEnabled(true);
            String fileName = getFileName(currentFileUri);

            String sectionName = iniStructureFragment.sectionName();
            if (sectionName != null) {
                String title = String.format(getString(R.string.titleWithSection), fileName, sectionName);
                actionBar.setTitle(title);
                actionBar.setSubtitle(R.string.linesSubtitle);
            } else {
                actionBar.setTitle(fileName);
                actionBar.setSubtitle(R.string.sectionsSubtitle);
            }
        } else {
            floatingActionButton.hide();

            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            iniStructureFragment = null;
            transaction.replace(R.id.activity_main_main_frame, new StartFragment());
            transaction.commit();

            actionBar.setDisplayHomeAsUpEnabled(false);
            actionBar.setTitle(getString(R.string.appName));
            actionBar.setSubtitle("");
        }
    }

    private String getFileName(Uri uri) {
        if (uri == null) {
            return getResources().getString(R.string.newFile);
        }

        String result = null;
        if (uri.getScheme().equals("content")) {

            try (Cursor cursor = getContentResolver().query(
                    uri, null, null, null, null)) {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        if (this.fileMenu == null) {
            this.fileMenu = menu;
        }
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        updateMenuItems();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_new_file: {
                currentFileUri = null;
                onNewFile();
                return true;
            }

            case R.id.action_open_file: {
                onOpenFile();
                return true;
            }

            case R.id.action_reopen_file: {
                onReopenFile();
                return true;
            }

            case R.id.action_save_file: {
                onSaveFile();
                return true;
            }

            case R.id.action_save_as_file: {
                onSaveAsFile();
                return true;
            }

            case R.id.action_close_file: {
                onCloseFile();
                return true;
            }

            case R.id.action_exit: {
                finish();
                return true;
            }

            case android.R.id.home: {
                onBackPressed();
            }

        }

        return super.onOptionsItemSelected(item);
    }

    public void onOpenFile() {
        onCloseFile();
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.setType("*/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(intent, REQUEST_TEXT_FILE_OPEN);
    }

    private void onReopenFile() {
        showCharsetDialog(REQUEST_CHARSET_FOR_FILE_OPEN);
    }

    private void showSnackbar(String string) {
        Snackbar.make(findViewById(R.id.main_activity_root), string, Snackbar.LENGTH_SHORT).show();
    }

    public void onNewFile() {
        showCharsetDialog(REQUEST_CHARSET_FOR_FILE_NEW);
    }

    private void onSaveFile() {
        if (!fileBuffer.isOpen()) {
            showSnackbar(getString(R.string.needToOpenFileSnackbarText));
            return;
        }

        if (fileBuffer.fileName() != null) {
            if (fileBuffer.save()) {
                showSnackbar(getString(R.string.saveFileSnackbarText));
            } else {
                showSnackbar(getString(R.string.failSaveFileSnackbarText));
            }
        } else {
            onSaveAsFile();
        }
    }

    private void onSaveAsFile() {
        if (!fileBuffer.isOpen()) {
            showSnackbar(getString(R.string.failSaveFileSnackbarText));
            return;
        }

        Intent intent = new Intent(Intent.ACTION_CREATE_DOCUMENT);
        intent.setType("*/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(intent, REQUEST_TEXT_FILE_SAVE);
    }

    private void onCloseFile() {
        fileBuffer.close();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_TEXT_FILE_OPEN
                || requestCode == REQUEST_TEXT_FILE_SAVE
                || requestCode == REQUEST_NONE) {

            if (resultCode != RESULT_OK) {
                return;
            }

            lastClosedActivityRequestCode = requestCode;
            currentFileUri = data.getData();
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    protected void onResume() {
        super.onResume();

        if (lastClosedActivityRequestCode == REQUEST_NONE) {
            return;
        }

        switch (lastClosedActivityRequestCode) {
            case REQUEST_TEXT_FILE_OPEN: {
                showCharsetDialog(REQUEST_CHARSET_FOR_FILE_OPEN);
                break;
            }

            case REQUEST_TEXT_FILE_SAVE: {
                if (fileBuffer.saveAs(
                        new AndroidFileStreamCreator(getBaseContext(), currentFileUri), charset)) {
                    showSnackbar(getString(R.string.saveFileSnackbarText));
                } else {
                    showSnackbar(getString(R.string.failSaveFileSnackbarText));
                }
                break;
            }
        }

        lastClosedActivityRequestCode = REQUEST_NONE;
    }

    private void updateMenuItems() {
        boolean isOpen = fileBuffer.isOpen();
        boolean isNeedSave = fileBuffer.isNeedSave();
        boolean hasFilename = fileBuffer.fileName() != null;

        fileMenu.findItem(R.id.action_new_file).setEnabled(true);
        fileMenu.findItem(R.id.action_open_file).setEnabled(true);
        fileMenu.findItem(R.id.action_reopen_file).setEnabled(isOpen);
        fileMenu.findItem(R.id.action_save_file).setEnabled(hasFilename && isNeedSave);
        fileMenu.findItem(R.id.action_save_as_file).setEnabled(isOpen);
        fileMenu.findItem(R.id.action_close_file).setEnabled(isOpen);
        fileMenu.findItem(R.id.action_exit).setEnabled(true);
    }

    public Text getText() {
        return fileBuffer.getText();
    }
}
