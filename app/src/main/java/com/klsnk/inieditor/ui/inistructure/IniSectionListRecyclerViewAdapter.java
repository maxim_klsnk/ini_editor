package com.klsnk.inieditor.ui.inistructure;

import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.klsnk.inieditor.R;
import com.klsnk.inieditor.core.ini.IniSectionList;

public class IniSectionListRecyclerViewAdapter
        extends RecyclerView.Adapter<IniSectionListRecyclerViewAdapter.ViewHolder> {

    public interface InteractionListener {
        void onFirstItemClick();
        void onItemClick(int position);
        void onItemRemove(int position);
        void onItemRename(int position);
        void onItemInsertNew(int position);
    }

    private final IniSectionList iniSectionList;
    private final InteractionListener interactionListener;

    IniSectionListRecyclerViewAdapter(IniSectionList iniSectionList,
                                      InteractionListener interactionListener) {
        this.iniSectionList = iniSectionList;
        this.interactionListener = interactionListener;

        iniSectionList.setOnSectionsChangeListener(new IniSectionList.OnSectionsChangeListener() {
            @Override
            public void onAddSection(String name) {
                notifyItemInserted(iniSectionList.sectionsCount());
            }

            @Override
            public void onInsertSection(int index, String name) {
                notifyItemInserted(index + 1);
                notifyItemRangeChanged(index + 2, iniSectionList.sectionsCount());
            }

            @Override
            public void onRenameSection(int index, String oldName, String newSection) {
                notifyItemChanged(index + 1);
            }

            @Override
            public void onRemoveSection(int index) {
                notifyItemRemoved(index + 1);
                notifyItemRangeChanged(index + 1, iniSectionList.sectionsCount());
            }
        });
    }

    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_ini_section, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        if (position == 0) {
            holder.sectionTextView.setText(R.string.freeSectionHeaderText);
            holder.view.setOnClickListener((View) -> interactionListener.onFirstItemClick());
            holder.menuButton.setVisibility(View.GONE);
        } else {
            int sectionPosition = position - 1;
            String sectionName = iniSectionList.getSectionName(sectionPosition);

            Resources resources = holder.sectionTextView.getResources();
            String sectionHeaderText = String.format(
                    resources.getString(R.string.sectionHeaderText),
                    sectionName);
            holder.sectionTextView.setText(sectionHeaderText);

            holder.view.setOnClickListener((View) ->
                    interactionListener.onItemClick(sectionPosition));

            holder.view.setOnLongClickListener((View) -> {
                openItemPopup(holder, sectionPosition);
                return true;
            });

            holder.menuButton.setOnClickListener(
                    (View view) -> openItemPopup(holder, sectionPosition));
        }
    }

    public void openItemPopup(ViewHolder holder, int sectionPosition) {
        PopupMenu popup = new PopupMenu(holder.menuButton.getContext(), holder.menuButton);
        popup.inflate(R.menu.menu_ini_section_item_options);
        popup.setOnMenuItemClickListener((MenuItem item) -> {
            switch (item.getItemId()) {
                case R.id.action_ini_section_options_rename: {
                    interactionListener.onItemRename(sectionPosition);
                    break;
                }

                case R.id.action_ini_section_options_insert_new: {
                    interactionListener.onItemInsertNew(sectionPosition);
                    break;
                }

                case R.id.action_ini_section_options_remove: {
                    interactionListener.onItemRemove(sectionPosition);
                    break;
                }
            }
            return true;
        });
        popup.show();
    }

    @Override
    public int getItemCount() {
        return iniSectionList.sectionsCount() + 1;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final View view;
        final TextView sectionTextView;
        final View menuButton;

        ViewHolder(View view) {
            super(view);
            this.view = view;
            this.sectionTextView = view.findViewById(R.id.item_text);
            this.menuButton = view.findViewById(R.id.button_ini_section_menu);
        }
    }
}
