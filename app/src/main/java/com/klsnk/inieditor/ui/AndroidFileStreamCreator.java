package com.klsnk.inieditor.ui;

import android.content.Context;
import android.net.Uri;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import com.klsnk.inieditor.core.files.FileStreamCreator;

public class AndroidFileStreamCreator implements FileStreamCreator {

    private Context context;
    private Uri uri;

    public AndroidFileStreamCreator(Context context, Uri uri) {
        this.context = context;
        this.uri = uri;
    }

    public InputStream openInputStream() throws IOException {
        return context.getContentResolver().openInputStream(uri);
    }

    public OutputStream openOutputStream() throws IOException {
        return context.getContentResolver().openOutputStream(uri);
    }

    public String fileName() {
        return uri.getPath();
    }
}
