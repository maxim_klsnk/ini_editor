package com.klsnk.inieditor.ui.inilinedialog.inilinefragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.klsnk.inieditor.R;
import com.klsnk.inieditor.core.ini.line.CommentIniLine;
import com.klsnk.inieditor.core.ini.line.IniLine;

public class CommentIniLineFragment extends IniLineFragment {

    private static final String COMMENT_KEY = "COMMENT_KEY";

    private EditText commentEditText;

    public void setIniLine(CommentIniLine commentIniLine) {
        Bundle arguments = new Bundle();
        arguments.putString(COMMENT_KEY, commentIniLine.getContent());
        setArguments(arguments);
    }

    public IniLine validate() {
        String comment = commentEditText.getText().toString();
        boolean ok = true;

        if (comment.isEmpty()) {
            commentEditText.setError(getString(R.string.commentIniLineError));
            ok = false;
        }


        return ok ? new CommentIniLine(comment) : null;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_comment_ini_line, container, false);

        commentEditText = (EditText)view.findViewById(R.id.ini_line_comment_edit_text);

        Bundle arguments = getArguments();
        if (arguments != null) {
            commentEditText.setText(arguments.getString(COMMENT_KEY), TextView.BufferType.EDITABLE);
        }

        return view;
    }
}
