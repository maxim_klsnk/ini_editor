package com.klsnk.inieditor.ui.inilinedialog.inilinefragments;

import com.klsnk.inieditor.core.ini.line.CommentIniLine;
import com.klsnk.inieditor.core.ini.line.EmptyIniLine;
import com.klsnk.inieditor.core.ini.line.IniLine;
import com.klsnk.inieditor.core.ini.line.KeyValueIniLine;

public class IniLineFragmentFacility {

    public static IniLineFragment getFragmentByTitleIndex(int index) {
        switch (index) {
            case 0: {
                return new KeyValueIniLineFragment();
            }

            case 1: {
                return new CommentIniLineFragment();
            }

            case 2: {
                return new EmptyLineIniLineFragment();
            }

            default: {
                return null;
            }
        }
    }

    public static IniLineFragment getFragmentByIniLine(IniLine iniLine) {
        if (iniLine instanceof KeyValueIniLine) {
            KeyValueIniLine keyValueIniLine = (KeyValueIniLine) iniLine;
            KeyValueIniLineFragment keyValueIniLineFragment = new KeyValueIniLineFragment();
            keyValueIniLineFragment.setIniLine(keyValueIniLine);
            return keyValueIniLineFragment;
        } else if (iniLine instanceof CommentIniLine) {
            CommentIniLine commentIniLine = (CommentIniLine) iniLine;
            CommentIniLineFragment commentIniLineFragment = new CommentIniLineFragment();
            commentIniLineFragment.setIniLine(commentIniLine);
            return commentIniLineFragment;
        } else if (iniLine instanceof EmptyIniLine) {
            return new EmptyLineIniLineFragment();
        } else {
            return null;
        }
    }

    public static int getTitleIndexByIniLine(IniLine iniLine) {
        if (iniLine instanceof KeyValueIniLine) {
            return 0;
        } else if (iniLine instanceof CommentIniLine) {
            return 1;
        } else if (iniLine instanceof  EmptyIniLine) {
            return 2;
        } else {
            return -1;
        }
    }
}
