package com.klsnk.inieditor.ui.inistructure;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.klsnk.inieditor.R;
import com.klsnk.inieditor.core.ini.IniLineList;
import com.klsnk.inieditor.core.ini.line.IniLine;
import com.klsnk.inieditor.androidaux.FragmentAux;
import com.klsnk.inieditor.ui.inilinedialog.IniLineEditDialogActivity;
import com.klsnk.inieditor.ui.inilinedialog.IniLineEditDialogActivity.ActionType;

import static com.klsnk.inieditor.ui.inilinedialog.IniLineEditDialogActivity.ActionType.EDIT;
import static com.klsnk.inieditor.ui.inilinedialog.IniLineEditDialogActivity.ActionType.INSERT;

public class IniLineListFragment
        extends Fragment
        implements IniLineListRecyclerViewAdapter.InteractionListener {

    public interface FragmentContext {
        IniLineList getIniLineList();
    }

    private IniLineList iniLineList;
    private RecyclerView recyclerView;
    private View emptyView;
    private int iniLineForEditIndex = -1;

    public IniLineListFragment() {
    }

    public void onAddButton() {
        onItemCreate();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == IniLineEditDialogActivity.RESULT_CANCEL) {
            return;
        }

        IniLineEditDialogActivity.ActionType dialogActionType
                = (IniLineEditDialogActivity.ActionType)data.getSerializableExtra(
                        IniLineEditDialogActivity.ACTION_TYPE_KEY);

        IniLine iniLine = (IniLine)data.getSerializableExtra(
                IniLineEditDialogActivity.INI_LINE_KEY);

        switch (dialogActionType) {
            case EDIT: {
                iniLineList.replaceLine(iniLineForEditIndex, iniLine);
                break;
            }

            case INSERT: {
                iniLineList.insertLine(iniLineForEditIndex, iniLine);
                break;
            }

            case ADD: {
                iniLineList.addLine(iniLine);
                break;
            }
        }
        iniLineForEditIndex = -1;
    }

    public void onItemClick(int position) {
        onItemEdit(position);
    }

    public void onItemEdit(int position) {
        openEditDialog(position, EDIT);
    }

    public void onItemInsertNew(int position) {
        openEditDialog(position, INSERT);
    }

    public void onItemCreate() {
        openEditDialog(-1, ActionType.ADD);
    }

    public void onItemRemove(int position) {
        iniLineList.removeLine(position);
    }

    public void onItemsSetChanged() {
        if (iniLineList.linesCount() > 0) {
            recyclerView.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
        } else {
            recyclerView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
        }
    }

    public void openEditDialog(int itemPosition,
                               IniLineEditDialogActivity.ActionType dialogActionType) {
        Intent intent = new Intent(getActivity(), IniLineEditDialogActivity.class);
        Bundle arguments = new Bundle();
        arguments.putSerializable(IniLineEditDialogActivity.ACTION_TYPE_KEY, dialogActionType);
        if (dialogActionType == EDIT) {
            IniLine iniLine = iniLineList.getLine(itemPosition);
            arguments.putSerializable(IniLineEditDialogActivity.INI_LINE_KEY, iniLine);
        }
        intent.putExtras(arguments);
        iniLineForEditIndex = itemPosition;
        startActivityForResult(intent, 0);
    }

    @SuppressWarnings("unused")
    public static IniLineListFragment newInstance(int columnCount) {
        return new IniLineListFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ini_line_list, container, false);

        FragmentContext fragmentContext = (FragmentContext) FragmentAux.getParent(this);

        iniLineList = fragmentContext.getIniLineList();

        Context context = view.getContext();
        recyclerView = view.findViewById(R.id.list);
        emptyView = view.findViewById(R.id.ini_list_empty);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(
                new IniLineListRecyclerViewAdapter(iniLineList, this));
        onItemsSetChanged();

        DividerItemDecoration dividerItemDecoration =
                new DividerItemDecoration(recyclerView.getContext(), LinearLayoutManager.VERTICAL);
        recyclerView.addItemDecoration(dividerItemDecoration);

        return view;
    }
}
