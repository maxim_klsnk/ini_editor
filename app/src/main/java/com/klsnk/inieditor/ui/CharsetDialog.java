package com.klsnk.inieditor.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

import com.klsnk.inieditor.androidaux.FlowActivity;

import java.util.ArrayList;

public class CharsetDialog extends DialogFragment {
    static public final String CHARSET_NAME_LIST_KEY = "CHARSET_NAME_LIST_KEY";
    static public final String SELECTED_CHARSET_INDEX_KEY = "SELECTED_CHARSET_INDEX_KEY";

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Bundle arguments = getArguments();
        final int requestCode = getTargetRequestCode();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Select encoding");


        DialogInterface.OnClickListener onClickListener = (DialogInterface dialog, int which) -> {
            Bundle resultData = new Bundle();
            resultData.putInt(SELECTED_CHARSET_INDEX_KEY, which);
            FlowActivity flowActivity = (FlowActivity)getActivity();
            flowActivity.receiveResult(requestCode, resultData);
            dismiss();
        };

        ArrayList<String> charsetNameList = arguments.getStringArrayList(CHARSET_NAME_LIST_KEY);
        String[] charsetNameArray = charsetNameList.toArray(new String[charsetNameList.size()]);

        builder.setItems(charsetNameArray, onClickListener);

        return builder.create();
    }
}
