package com.klsnk.inieditor.ui.inilinedialog.inilinefragments;

import android.support.v4.app.Fragment;

import com.klsnk.inieditor.core.ini.line.IniLine;

public abstract class IniLineFragment extends Fragment {
    public abstract IniLine validate();
}
