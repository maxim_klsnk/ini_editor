package com.klsnk.inieditor.ui.inistructure;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.klsnk.inieditor.R;
import com.klsnk.inieditor.core.ini.IniLineList;
import com.klsnk.inieditor.core.ini.IniModel;
import com.klsnk.inieditor.core.ini.IniSectionList;
import com.klsnk.inieditor.core.text.Text;
import com.klsnk.inieditor.androidaux.FlowFragment;

public class IniStructureFragment extends FlowFragment
    implements IniLineListFragment.FragmentContext, IniSectionListFragment.FragmentContext {

    public interface FragmentContext {
        Text getText();
        void onOpenSection();
        void onCloseSection();
    }

    private IniModel iniModel;
    private IniLineList targetIniLineList;
    private FragmentContext fragmentContext;
    private IniSectionListFragment iniSectionListFragment;
    private IniLineListFragment iniLineListFragment;
    private String currentSectionName;
    private boolean isRequireNavigateBack = false;

    public IniStructureFragment () {
    }

    public void onAddButton() {
        if (isRequireNavigateBack) {
            iniLineListFragment.onAddButton();
        } else {
            iniSectionListFragment.onAddButton();
        }
    }

    public boolean isRequireNavigateBack() {
        return isRequireNavigateBack;
    }

    public String sectionName() {
        return currentSectionName;
    }

    public void navigateBack() {

        iniSectionListFragment = new IniSectionListFragment();
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.replace(R.id.ini_structure_frame, iniSectionListFragment);
        transaction.commit();
        isRequireNavigateBack = false;

        if (currentSectionName != null) {
            currentSectionName = null;
            fragmentContext.onCloseSection();
        }
    }

    public IniLineList getIniLineList() {
        return targetIniLineList;
    }

    public IniSectionList getIniSectionList() {
        return iniModel;
    }

    public void onShowIniLineList(IniLineList iniLineList) {
        targetIniLineList = iniLineList;
        iniLineListFragment = new IniLineListFragment();
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.replace(R.id.ini_structure_frame, iniLineListFragment);
        transaction.addToBackStack(IniLineListFragment.class.getName());
        transaction.commit();
        isRequireNavigateBack = true;
    }

    public void onOpenSection(String sectionName) {
        currentSectionName = sectionName;
        fragmentContext.onOpenSection();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ini_structure, container, false);

        iniModel = new IniModel(fragmentContext.getText());

        iniSectionListFragment = new IniSectionListFragment();
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.add(R.id.ini_structure_frame, iniSectionListFragment);
        transaction.commit();

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        fragmentContext = (FragmentContext) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        fragmentContext = null;
    }
}
